# -*- coding: utf-8 -*-
import scrapy

from ..items import NcbiItem


class Ncbi1Spider(scrapy.Spider):
    name = "ncbi1"
    allowed_domains = ["ncbi.nlm.nih.gov"]
    start_urls = (
        'http://www.ncbi.nlm.nih.gov/pubmed/?term=The%20cost-effectiveness%20of%20mirtazapine%20versus%20paroxetine%20in%20treating%20people%20with%20depression%20in%20primary%20care',
    )

    def parse(self, response):
        urls = response.xpath('//div[@id="maincontent"]//a/@href').extract()
        for url in urls:
            if not url.startswith('#'):
                yield NcbiItem(url=url)
