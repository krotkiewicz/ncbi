# -*- coding: utf-8 -*-

# Scrapy settings for ncbi project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'ncbi'

SPIDER_MODULES = ['ncbi.spiders']
NEWSPIDER_MODULE = 'ncbi.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'ncbi (+http://www.yourdomain.com)'

DOWNLOADER_MIDDLEWARES = {
    'ncbi.rotate_useragent.RotateUserAgentMiddleware' : 400,
}
